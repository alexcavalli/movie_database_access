## Heroku link
http://intense-scrubland-48712.herokuapp.com/

### Things that are obviously missing, broken, or not-exactly-ideal:

* It would be nice if the movie details were hidden until a movie was chosen, and re-hidden when a new search was executed or the faves were displayed. This wouldn't be too hard but seemed tangential to the assignment.
* Search results only shows page 1 with no indication that there are additional pages.
* Favorites should probably disappear while viewing favorites, when the box is unchecked.
* Promises would help with some of the callback spaghetti
* Could improve this a bit more using modules

### React

I haven't had the opportunity to rewrite this with React, but here are some thoughts on how it might compare.

#### Pros:

* Compartmentalizes presentation code nicely
* Better encapsulation of UI state with related elements
* Cleaner event bindings to elements

#### Cons:

* Somewhat hides what is going on, which can be a good and a bad thing to a new learner
* Is probably overkill for a situation as simple as this one
* More code being included into the app payload
