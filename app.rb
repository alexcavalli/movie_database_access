require 'sinatra'
require 'json'

get '/' do
  File.read(File.join('views', 'index.html'))
end

get '/favorites' do
  response.header['Content-Type'] = 'application/json'
  File.read('data.json')
end

post '/favorites' do
  return 'Invalid Request' unless params[:name] && params[:oid]

  movie = { name: params[:name], oid: params[:oid] }

  update_favorites do |favorites|
    favorites << movie
  end
  movie.to_json
end

delete '/favorites' do
  return 'Invalid Request' unless params[:name] && params[:oid]

  movie = { name: params[:name], oid: params[:oid] }

  update_favorites do |favorites|
    favorites.delete(movie)
    favorites
  end
  movie.to_json
end

# Loads favorites from the filesystem, passes it to the provided block, and
# saves the block response back to the filesystem. The provided block should
# return an updated set of favorites.
#
# Note: This could have been written to directly rely on the mutation done in
# the block, but I found this potentially confusing and chose to instead rely on
# the block response.
def update_favorites
  raise ArgumentError, 'Block is required' unless block_given?

  favorites_json = File.read('data.json')
  favorites = JSON.parse(favorites_json, symbolize_names: true)
  new_favorites = yield favorites
  File.write('data.json', JSON.pretty_generate(new_favorites))
end
