// Bind events when the document has loaded
$(document).ready(function () {
  // Bind search form submission
  $('#search').submit(UIEvents.search)

  // Bind the View Favorites link
  $('#show-favorites').click(UIEvents.showFavorites)

  // These events are bound to a parent element because the triggering elements
  // will not exist at the time this binding is set up. An element must exist in
  // the DOM in order to bind an event to it.
  $('#movies').click('.btn-show-movie-details', UIEvents.showDetails)
  $('#movies').click('.chk-fave', UIEvents.toggleFave)
})

// Event handlers
var UIEvents = {
  // Renders a table with search results based on the search term.
  search: function (event) {
    // Stop the form from causing a server-side page refresh.
    // We do this because we are handling the result of the form submission in
    // javascript.
    event.preventDefault()

    var searchTarget = $('#search-target').val()

    // validate input
    if (searchTarget.length < 2) {
      $('#search-target-form-group').addClass('has-error')
      $('#search-target-help-text').removeClass('hide')
      return false
    }

    // clear any errors that may have already been displayed
    $('#search-target-form-group').removeClass('has-error')
    $('#search-target-help-text').addClass('hide')

    OMDB.search(searchTarget, function (searchData) {
      Favorites.fetchFaves(function (favesData) {
        var favedIds = Favorites.extractFavedIds(favesData)
        var moviesHtml = UIHelpers.formatMovies(searchData.Search, favedIds)
        $('#movies').html(moviesHtml)
      })
    })
  },

  // Renders a box showing details on the selected movie.
  showDetails: function (event) {
    // grab the imdb id from the data-imdb-id property of the clicked-on link.
    var imdbId = $(event.target).data('imdbId')

    OMDB.movieDetails(imdbId, function (movieData) {
      $('#selected-movie-title').text(movieData.Title)
      $('#selected-movie-director').text(movieData.Director)
      $('#selected-movie-genre').text(movieData.Genre)
      $('#selected-movie-year').text(movieData.Year)
      $('#selected-movie-rated').text(movieData.Rated)
      $('#selected-movie-runtime').text(movieData.Runtime)
      $('#selected-movie-plot').text(movieData.Plot)
    })
  },

  // Renders a table of favorites movies.
  showFavorites: function () {
    Favorites.fetchFaves(function (favesData) {
      // mimic an OMDB response with our faves data
      var moviesData = favesData.map(function (faveData) {
        return {'Title': faveData.name, 'imdbID': faveData.oid}
      })

      // also compute the faved ids list from the faves data
      // note: we know that every item here is faved (we are showing favorites)
      // but this allows the reuse of the existing movie formatting code
      var favedIds = Favorites.extractFavedIds(favesData)

      // render the favorites to the table
      var moviesHtml = UIHelpers.formatMovies(moviesData, favedIds)
      $('#movies').html(moviesHtml)
    })
  },

  // Toggles the faved status of a movie.
  toggleFave: function (event) {
    // get the checkbox that triggered the event
    var $checkbox = $(event.target)

    // get the checked status of the box
    var isChecked = $checkbox.is(':checked')

    // get the movie object that the box is connected to
    var movie = {
      oid: $checkbox.data('movieOid'),
      name: $checkbox.data('movieName')
    }

    // issue an API call to update the faved status on the backend
    if (isChecked) {
      Favorites.addFave(movie)
    } else {
      Favorites.removeFave(movie)
    }
  }
}

// UI rendering and helpers
var UIHelpers = {
  // Constructs table content from an OMDB response dataset (an array of
  // objects) and the provided faved ids. Returns the built table content.
  formatMovies: function (moviesData, favedIds) {
    var tableHeader = '<thead><tr><th>Title</th><th>Fave</th></tr></thead>'
    var tableRows = moviesData.map(function (movieData) {
      return '<tr><td>' + UIHelpers.titleLink(movieData) + '</td><td>' + UIHelpers.faveCheckbox(movieData, favedIds) + '</td></tr>'
    })
    var combinedHtml = (tableHeader + '<tbody>' + tableRows.join('\n') + '</tbody>')
    return combinedHtml
  },

  // Builds a link for this title to be used to show additional details on this
  // movie.
  titleLink: function (movieData) {
    return "<button class='btn-show-movie-details btn btn-link' data-imdb-id='" + movieData.imdbID + "'>" + movieData.Title + '</button>'
  },

  // Renders a checkbox that is checked if this movie is currently faved.
  faveCheckbox: function (movieData, favedIds) {
    // check if this movie's id appears in the faved ids list
    var isFaved = (favedIds.indexOf(movieData.imdbID) !== -1)

    // set the value for the "checked" property of the checkbox based on faved status
    var checkedVal = (isFaved ? 'checked' : '')

    return "<input class='chk-fave' data-movie-oid='" + movieData.imdbID + "' data-movie-name='" + movieData.Title + "' type='checkbox' " + checkedVal + '>'
  }
}

// Local API calls and other methods to fetch and manipulate favorites data
var Favorites = {
  // Fetches current set of faved movies and passes it to the callback function.
  fetchFaves: function (callback) {
    $.get(
      'favorites',
      {},
      callback
    )
  },

  // Adds a movie to the faved movies list. No return value.
  addFave: function (movie) {
    $.post(
      'favorites',
      movie
    )
  },

  // Removes a movie to the faved movies list. No return value.
  removeFave: function (movie) {
    $.ajax(
      'favorites',
      {
        data: movie,
        method: 'DELETE'
      }
    )
  },

  // Builds an array of faved movie ids from faves data.
  extractFavedIds: function (favesData) {
    return favesData.map(function (faveData) {
      return faveData.oid
    })
  }
}

// OMDB API calls
var OMDB = {
  // Searches for searchTarget on OMDB and passes result to the callback function.
  search: function (searchTarget, callback) {
    $.get(
      'http://www.omdbapi.com/',
      {s: searchTarget},
      callback
    )
  },

  // Fetches movie details by imdbID and passes it to the callback function.
  movieDetails: function (imdbId, callback) {
    $.get(
      'http://www.omdbapi.com/',
      {i: imdbId},
      callback
    )
  }
}

// OMDB API details (for reference only)

// For a movie fetch by ID, individual movie objects have this format:
// {
//   "Title":"Star Wars: Episode IV - A New Hope",
//   "Year":"1977",
//   "Rated":"PG",
//   "Released":"25 May 1977",
//   "Runtime":"121 min",
//   "Genre":"Action, Adventure, Fantasy",
//   "Director":"George Lucas",
//   "Writer":"George Lucas",
//   "Actors":"Mark Hamill, Harrison Ford, Carrie Fisher, Peter Cushing",
//   "Plot":"Luke Skywalker joins forces with a Jedi Knight, a cocky pilot, a wookiee and two droids to save the galaxy from the Empire's world-destroying battle-station, while also attempting to rescue Princess Leia from the evil Darth Vader.",
//   "Language":"English",
//   "Country":"USA",
//   "Awards":"Won 6 Oscars. Another 39 wins & 28 nominations.",
//   "Poster":"http://ia.media-imdb.com/images/M/MV5BOTIyMDY2NGQtOGJjNi00OTk4LWFhMDgtYmE3M2NiYzM0YTVmXkEyXkFqcGdeQXVyNTU1NTcwOTk@._V1_SX300.jpg",
//   "Metascore":"92",
//   "imdbRating":"8.7",
//   "imdbVotes":"901,351",
//   "imdbID":"tt0076759",
//   "Type":"movie",
//   "Response":"True"
// }
//
// For a movie search, individual movie objects have this format:
// {
//   "Title":"Star Wars: Episode IV - A New Hope","Year":"1977",
//   "imdbID":"tt0076759",
//   "Type":"movie",
//   "Poster":"http://ia.media-imdb.com/images/M/MV5BOTIyMDY2NGQtOGJjNi00OTk4LWFhMDgtYmE3M2NiYzM0YTVmXkEyXkFqcGdeQXVyNTU1NTcwOTk@._V1_SX300.jpg"
// }
